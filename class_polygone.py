#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 14:05:34 2020

@author: c19012324
"""

import class_point as p
import math as m

class Polygone:

    def __init__(self, sommets):
        self.sommets =sommets
        
    def len_sommet(self):
        return len(self.sommets)     

    def get_sommet(self, i):
        return self.sommets[i]
    
   #pas fini
    def aire(self):
        list_x=[]
        list_y=[]
        
        #rempli d'une liste de x et une liste de y 
        for i in range(0,len(self.sommets)):
            #self.sommets[i].x appel le x du point car sommets est un type point on 
            #peut aussi faire self.sommets[i].x() quand j'aurai mis les variable en privé et bien mit la fonction 
            list_x.append(self.sommets[i].x)
            list_y.append(self.sommets[i].y)
            
        print("x  ",list_x,"y ", list_y )
        taille=len(list_x)*2
        i=0
        air_x=[]
        air_y=[]
        #rempli la list air 
        while i < len(list_x)-1:
            air_x.append(list_x[i] +  list_x[i+1])
            air_y.append(abs(list_y[i] -  list_y[i+1]))
            
            i=i+1
            
            #avance de deux en deux pour ne pas prendre deux fois la meme valeur
            
        
        print("air x",air_x,"air y ",air_y)
        
        final_air=[]
        for i in range(0,len(air_x)):
                final_air.append(air_x[i] + air_y[i])
			
        #print("FINAL",final_air)
        
        return final_air
       
    
    def __str__(self):
        text="["
        self.len_sommet()
        for i in range (0,self.len_sommet()):
            text =  text + str(self.sommets[i])        
        return text+']'
  

#class triangle herite de polygone
class Triangle(Polygone):  
    def __init__(self,a,b,c):
        liste_triangle=[a,b,c]
        Polygone.__init__(self,liste_triangle)
        

    #quand on print  un objet de type Trangle on aura le str de la class polygone qui sera appeler 
    #car on n'a pas declarer de str dans la class Triangle        
       
        
#class rectangle herite de polygone
class Rectangle(Polygone):
    def __init__(self,xMin,yMin,xMax,yMax):
        liste_rectangle=[xMin,yMin,xMax,yMax]
        
        if yMin.x != xMin.x or yMax.x != xMax.x : # pas la peine de mettre les autres coordonée condition avec min
                #print("faux")
                raise Exception("Ne respecte pas la propiete de parallèle aux axes du rectangle")
        else:
                Polygone.__init__(self,liste_rectangle)




#class PolygoneRegulier herite de Polygone
class PolygoneRegulier(Polygone):
	def __init__(self,centre,rayon,nombreSommets):

		self.c=centre
		self.r=rayon
		self.n=Polygone.__init__(self,nombreSommets)
		

			
			
	def x_PR(self,i):
		
		x_temp=Polygone.get_sommet(self,i).x
		n_len=Polygone.len_sommet(self)
		x= x_temp+ self.r + m.cos(2* m.pi * (i/ n_len))
		return x
		
	def y_PR(self,i):
		
		y_temp=Polygone.get_sommet(self,i).y
		n_len=Polygone.len_sommet(self)
		y= y_temp+ self.r + m.cos(2* m.pi * (i/ n_len))
		return y
		
		
		
		
 
             
#####Class point
point_un = p.Point(1,2)

print(point_un.x)
print(point_un.y)
print(point_un.r())
print(point_un.t())
print(point_un.__str__()) 

point_deux =p.Point(1,4)


print(point_un.__eq__(point_deux))  
print(point_un.homothetie(2))    
print(point_un.translation(1,2))  
print(point_un.rotation(2))

point_trois =p.Point(3,2)            
point_quatre =p.Point(3,4)    
     
#sommets doit etre une liste de points        
liste_point=[point_un,point_deux,point_trois,point_quatre]

print ('#########POLYGONE##############') 
polygone_un=Polygone(liste_point)

#affiche les adresses
print(str(polygone_un))

#print("Récuperer  sommet",polygone_un.get_sommet()    )
print("Air ",polygone_un.aire( ) )    
    
print ('#########TRIANGLE##############') 
#faut donner une liste de point
triangle_un=Triangle(liste_point[0],liste_point[1],liste_point[2])
print(str(triangle_un))  
    
print ('#########RECTANGLE##############')   
rectangle_un=Rectangle(liste_point[0],liste_point[1],liste_point[2],liste_point[3])
print(str(rectangle_un))   

print ('#########POLYGONE_REGULIER##############')   
polygonergegulier_un=PolygoneRegulier(2,3,liste_point)
print(str(polygonergegulier_un))   

print("un x :",polygonergegulier_un.x_PR(1))  
print("un y :",polygonergegulier_un.y_PR(1))      
    
    
    
    
    
    
    
    
